import React, {useState} from "react";
import { v4 as uuidv4 } from 'uuid';
import "./Todo/Todo.css"
import AddTaskForm from "./Todo/AddTaskForm";
import Task from "./Todo/Task";

function App() {
    const [tasks, setTasks] = useState([
        {id: uuidv4(), text: "Buy chips"},
        {id: uuidv4(), text: "Watch good film"},
        {id: uuidv4(), text: "Think about life"},
    ]);

    const [currentTask, setCurrentTask] = useState("");

    const handleText = (event) => {
        setCurrentTask(event.target.value)
    };

    const addNewTask = () => {
        setTasks([
                ...tasks,
                {id: uuidv4(), text: currentTask}
            ]
        );
    };

    const removeTask = id => {
        setTasks(tasks.filter(task => task.id !== id));
    };

    const tasksComponents = tasks.map(task => {
        return (
            <Task text={task.text} key={task.id} remove={() => removeTask(task.id)}/>
        )
    });

  return (
    <div className="container">
      <AddTaskForm onTextChange={handleText} createTask={addNewTask}/>
        {tasksComponents}
    </div>
  );
}

export default App;