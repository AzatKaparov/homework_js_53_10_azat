import React from 'react';


const AddTaskForm = ({ onTextChange, createTask }) => {
    return (
        <div>
            <form action="#" className="main-form">
                <input
                    className="main-input"
                    type="text"
                    placeholder="Add new task"
                    onChange={onTextChange}
                />
                <button onClick={createTask} id="addBtn" type="button">Add</button>
            </form>
        </div>
    );
};

export default AddTaskForm;