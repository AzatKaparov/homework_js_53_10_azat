import React from 'react';

const Task = ({ text, remove }) => {
    return (
        <div className="task">
            <p className="task-text">{text}</p>
            <button onClick={remove} type="button" className="task-delete">Delete</button>
        </div>
    );
};

export default Task;